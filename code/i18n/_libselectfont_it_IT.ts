<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>SelectFont</name>
    <message>
        <source>Select font</source>
        <translation>Seleziona font</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation>Normale</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation>Grassettto</translation>
    </message>
    <message>
        <source>Choose the type of font:&lt;br&gt;&quot;Monospace fonts&quot;, fonts where all characters are the same width. &lt;br&gt;&quot;Proportional fonts&quot;, fonts where the width varies depending on the character.</source>
        <translation>Scegli il tipo di carattere:&lt;br&gt;&quot;Caratteri monospazio&quot;, caratteri in cui tutti i caratteri hanno la stessa larghezza. &lt;br&gt;&quot;Caratteri proporzionali&quot;, caratteri la cui larghezza varia a seconda del carattere.</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <source>Italic</source>
        <translation>Corsivo</translation>
    </message>
    <message>
        <source>Bold and italic</source>
        <translation>Grassetto/corsivo</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Predefiniti</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <source>All fonts</source>
        <translation>Tutte le font</translation>
    </message>
    <message>
        <source>Monospaced fonts</source>
        <translation>Font monospazio</translation>
    </message>
    <message>
        <source>Proportional fonts</source>
        <translation>Font proporzionali</translation>
    </message>
    <message>
        <source>fonts</source>
        <translation>caratteri</translation>
    </message>
</context>
</TS>
