//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          libselectfont
//          Copyright (C) 2021 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/libselectfont
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
/*
  QFontDef comparison is more complicated than just simple
  per-member comparisons.

  When comparing point/pixel sizes, either point or pixelsize
  could be -1.  in This case we have to compare the non negative
  size value.

  This test will fail if the point-sizes differ by 1/2 point or
  more or they do not round to the same value.  We have to do this
  since our API still uses 'int' point-sizes in the API, but store
  deci-point-sizes internally.

  To compare the family members, we need to parse the font names
  and compare the family/foundry strings separately.  This allows
  us to compare e.g. "Helvetica" and "Helvetica [Adobe]" with
  positive results.
*/
#include "selectfont.h"

#include "ui_selectfont.h"

#include <QFile>

#include <QStyleHints>
#include <QFont>
#include <QApplication>



SelectFont::SelectFont(QWidget *parent) : QDialog(parent), ui(new Ui::SelectFont)
{
#if defined Q_OS_WIN
#if QT_VERSION < QT_VERSION_CHECK(5, 6, 3)
    QFont font(QFont(QStringLiteral(":/fonts/tahoma.ttf")));
    font.setPointSize(FONTSIZE);
    this->setFont(font);
#endif
#if QT_VERSION > QT_VERSION_CHECK(5, 6, 2)
    QFont font(QFont(QStringLiteral(":/fonts/segoeui.ttf")));
    font.setPointSize(FONTSIZE);
    this->setFont(font);
#endif
#elif defined Q_OS_LINUX
    QFont font(QFont(QStringLiteral(":/fonts/Ubuntu-R.ttf")));
    font.setPointSize(FONTSIZE);
    this->setFont(font);
#endif
    ui->setupUi(this);
    this->resize(0, 0);
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
#endif
    // foreach(QWidget *widget, QApplication::allWidgets()) {
    //     widget->setFont(QApplication::font());
    //     widget->update();
    // }
    ui->normal->setFont(QApplication::font());
    ui->bold->setFont(QApplication::font());
    ui->italic->setFont(QApplication::font());
    ui->boldItalic->setFont(QApplication::font());
    /** ICON **/
    QIcon iconWindow_close;
    QIcon iconDefault;
#if defined(Q_OS_LINUX) && (QT_VERSION > QT_VERSION_CHECK(6, 0, 0))
    iconWindow_close = QIcon::fromTheme(QIcon::ThemeIcon::WindowClose);

    if(iconWindow_close.isNull()) {
        iconWindow_close = QIcon(QStringLiteral(":/custom-icons/application-exit.png"));
    }

#else
    iconWindow_close = QIcon(QStringLiteral(":/custom-icons/application-exit.png"));
#endif
    ui->pbExit->setIcon(iconWindow_close);
    iconDefault = QIcon(QStringLiteral(":/custom-icons/default.png"));
    ui->pbDefault->setIcon(iconDefault);
    run();  //  line 241
}




void SelectFont::sFont(QString displayName, QString executableName, QIcon icon)
{
    QApplication::setWindowIcon(icon);
    sFont(displayName, executableName);
}

void SelectFont::sFont(QString displayName, QString executableName)
{
    QFont systemfont = QGuiApplication::font();
    ui->bold->setStyleSheet("QRadioButton { font-weight:bold}");
    ui->italic->setStyleSheet("QRadioButton { font-style:italic}");
    ui->boldItalic->setStyleSheet("QRadioButton { font-weight:bold; font-style:italic;}");
    display_name = displayName;
    executable_name = executableName;
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, displayName,
                       executableName);
    QFile settingsfile(settings.fileName());

    if(!settingsfile.exists()) {
        ui->pbDefault->animateClick();
    }

    settings.beginGroup(QStringLiteral("Font"));
    QString family = settings.value(QStringLiteral("family"), systemfont).toString();
    CURRENTFONTSIZE = settings.value("size", FONTSIZE).toInt();
    bool bold = settings.value(QStringLiteral("bold"), false).toBool();
    bool italic = settings.value(QStringLiteral("italic"), false).toBool();
    bool boldItalic = settings.value(QStringLiteral("boldItalic"), false).toBool();
    QFont currentfont = settings.value(QStringLiteral("currentfont"), systemfont).value<QFont>();
    int fonttype = settings.value(QStringLiteral("fonttype"), 0).toInt();
    settings.endGroup();
    ui->fontTextEdit->setFont(currentfont);

    if(boldItalic) {
        ui->boldItalic->setChecked(true);
    } else if(bold) {
        ui->bold->setChecked(true);
    } else if(italic) {
        ui->italic->setChecked(true);
    } else {
        ui->normal->setChecked(true);
    }

    ui->comboFontFamilies->setCurrentFont(family);
    /* comboFilter */
    QStringList filter = {tr("All fonts"), tr("Monospaced fonts"),
                          tr("Proportional fonts")
                         };
    QList<QFontComboBox::FontFilter> qfilter = {QFontComboBox::AllFonts,
                                                QFontComboBox::MonospacedFonts,
                                                QFontComboBox::ProportionalFonts,
                                               };
    ui->comboFilter->addItems(filter);
    ui->comboFilter->setCurrentIndex(fonttype);
    ui->lblCount->setText(QString::number(ui->comboFontFamilies->count()) + QStringLiteral(" ") + tr("fonts"));
    ui->fontTextEdit->setReadOnly(false);
    ui->comboFontFamilies->setCurrentFont(family);
    this->show();
    connect(ui->comboFilter,
            static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this,
    [this, qfilter, systemfont]() -> void {

        ui->comboFontFamilies->setMaxCount(1);
        ui->comboFontFamilies->setFontFilters(
            qfilter.at(ui->comboFilter->currentIndex()));




        for(int i = 0; i < ui->comboFontFamilies->count(); i++) {
            if(ui->comboFontFamilies->currentText().contains(QStringLiteral("8514oem"))) {
                ui->comboFontFamilies->removeItem(i);
            }
        }
        ui->lblCount->setText(QString::number(ui->comboFontFamilies->count()) + QStringLiteral(" ") + tr("fonts"));

        triggerSignal();

    });
    ui->comboFontSizes->setCurrentText(QString::number(CURRENTFONTSIZE));
    connect(ui->comboFontFamilies, &QFontComboBox::currentFontChanged, this,
            [this]() -> void { triggerSignal(); });
    /* connect comboFontSizes */
    connect(
        ui->comboFontSizes,
        static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this,
        [this]() -> void { triggerSignal(); });
    /*    */
    /* connect normal, bold, bolditalic */
    connect(ui->normal, &QAbstractButton::clicked, this, [this]() {
        triggerSignal();
    });

    connect(ui->bold, &QAbstractButton::clicked, this, [this]() {
        triggerSignal();
    });

    connect(ui->italic, &QAbstractButton::clicked, this, [this]() {
        triggerSignal();
    });

    connect(ui->boldItalic, &QAbstractButton::clicked, this, [this]() {
        triggerSignal();
    });

    connect(ui->comboFilter, &QComboBox::currentTextChanged, this, [this]() {
        triggerSignal();
    });

    /* Exit */
    connect(ui->pbExit, &QAbstractButton::clicked, this, [this]() -> void {

        delete this;
    });
    /* Default */ // ipac
    QIcon iconDefault;
#if defined(Q_OS_LINUX) && (QT_VERSION > QT_VERSION_CHECK(6, 0, 0))
    iconDefault = QIcon::fromTheme(QStringLiteral("emblem-default"));

    if(iconDefault.isNull()) {
        iconDefault = QIcon(QStringLiteral(":/custom-icons/default.png"));
    }

#else
    iconDefault = QIcon(QStringLiteral(":/custom-icons/default.png"));
#endif
    ui->pbDefault->setIcon(iconDefault);
    connect(ui->pbDefault, &QAbstractButton::clicked, this, [this]() -> void {


        ui->comboFilter->setCurrentIndex(0);
        ui->comboFontSizes->setCurrentText(QString::number(FONTSIZE));
        ui->normal->setChecked(true);
        ui->fontTextEdit->setCurrentFont(QGuiApplication::font());
        ui->comboFontFamilies->setCurrentFont(QGuiApplication::font());
        ui->pbExit->setFocus();
        triggerSignal();

    });
}

void SelectFont::triggerSignal()
{
    QString currentfont = ui->comboFontFamilies->currentText();
    QString size = ui->comboFontSizes->currentText();
    QFont font = QFont(currentfont, size.toInt());

    if(ui->boldItalic->isChecked()) {
        font.setWeight(QFont::Bold);
        font.setStyle(QFont::StyleItalic);
    } else {
        if(ui->bold->isChecked()) {
            font.setWeight(QFont::Bold);
            font.setStyle(QFont::StyleNormal);
        } else if(ui->italic->isChecked()) {
            font.setStyle(QFont::StyleItalic);
            font.setWeight(QFont::Normal);
        } else if(ui->normal->isChecked()) {
            font.setWeight(QFont::Normal);
            font.setStyle(QFont::StyleNormal);
        }
    }

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, display_name,
                       executable_name);
    settings.beginGroup(QStringLiteral("Font"));
    settings.setValue(QStringLiteral("family"), ui->comboFontFamilies->currentText());
    settings.setValue(QStringLiteral("size"), ui->comboFontSizes->currentText());
    settings.setValue(QStringLiteral("boldItalic"), ui->boldItalic->isChecked());
    settings.setValue(QStringLiteral("bold"), ui->bold->isChecked());
    settings.setValue(QStringLiteral("italic"), ui->italic->isChecked());
    settings.setValue(QStringLiteral("currentfont"), font);
    settings.setValue(QStringLiteral("fonttype"), ui->comboFilter->currentIndex());
    settings.endGroup();
    settings.sync();
    ui->fontTextEdit->setFont(font);
    emit valueChanged(font);
}


void SelectFont::run()
{
#if QT_VERSION > QT_VERSION_CHECK(5, 6, 2)
    QIcon::setFallbackSearchPaths(QIcon::fallbackSearchPaths() << ":/custom-icons");
#endif
    QStringList fontSizes = {QStringLiteral("6"),  QStringLiteral("7"),  QStringLiteral("8"),  QStringLiteral("9"),  QStringLiteral("10"), QStringLiteral("11"), QStringLiteral("12"), QStringLiteral("13"), QStringLiteral("14"),
                             QStringLiteral("15"), QStringLiteral("16"), QStringLiteral("17"), QStringLiteral("18"), QStringLiteral("19"), QStringLiteral("20"), QStringLiteral("21"), QStringLiteral("22"), QStringLiteral("23"),
                             QStringLiteral("24"), QStringLiteral("25"), QStringLiteral("26"), QStringLiteral("27"), QStringLiteral("28")
                            };
    ui->comboFontSizes->addItems(fontSizes);
    // Q_INIT_RESOURCE
    // QFont font = ui->fontTextEdit->font();
    ui->fontTextEdit->setText(QStringLiteral("Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."));
    // this->show();
}

void SelectFont::closeEvent(QCloseEvent *e)
{
    emit isClosing(true);
    e->accept();
}

SelectFont::~SelectFont()
{
    emit isClosing(true);
}
