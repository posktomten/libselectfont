
//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          libselectfont
//          Copyright 2021 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/libselectfont
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#if !defined(SELECTFONT_H)
#define SELECTFONT_H


#if !defined(QT_STATIC)
#include "selectfont_global.h"
#endif // QT_STATIC
#include <QDebug>
#include <QDialog>
#include <QSettings>
#include <QWhatsThis>
#include <QCloseEvent>
#ifdef Q_OS_LINUX
#define FONTSIZE 10
#endif
#ifdef Q_OS_WIN
#define FONTSIZE 10
#endif


QT_BEGIN_NAMESPACE
namespace Ui
{
class SelectFont;
}

QT_END_NAMESPACE



#if defined(Q_OS_LINUX)
#if defined(SELECTFONT_LIBRARY)
#include "selectfont_global.h"
class SELECTFONT_EXPORT SelectFont : public QDialog
#else
class SelectFont : public QDialog
#endif
#else
class SelectFont : public QDialog
#endif // Q_OS_LINUX
{

    Q_OBJECT

private:
    Ui::SelectFont *ui;
    void triggerSignal();
    QString display_name;
    QString executable_name;
    QFont CURRENTFONT;
    int CURRENTFONTSIZE;
    void stateChenged(int state);
    void run();
    void closeEvent(QCloseEvent *e) override;

public:
    explicit SelectFont(QWidget *parent = nullptr);

    ~SelectFont();
    void sFont(QString display_name, QString executable_name);
    void sFont(QString display_name, QString executable_name, QIcon icon);

signals:
    void valueChanged(QFont font);
    void isClosing(bool);


};

#endif // SELECTFONT_H
