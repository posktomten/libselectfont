
# 2021-05-18
QT += core gui network widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
#CONFIG+=staticlib

HEADERS += \
    selectfont.h \
    selectfont_global.h

SOURCES += \
    selectfont.cpp

FORMS += \
    selectfont.ui

#Export LIBS
DEFINES += SELECTFONT_LIBRARY

TEMPLATE = lib

# By default, qmake will make a shared library. Uncomment to make the library
# static.
#win32:CONFIG += staticlib
CONFIG+=staticlib

#CONFIG += lrelease embed_translations

# By default, TARGET is the same as the directory, so it will make 
# liblibrary.so or liblibrary.a (in linux).  Uncomment to override.

CONFIG(release, debug|release):BUILD=RELEASE
CONFIG(debug, debug|release):BUILD=DEBUG


equals(BUILD,RELEASE) {

    TARGET=selectfont

}
equals(BUILD,DEBUG) {

    TARGET=selectfontd
}

equals(QT_MAJOR_VERSION, 5) {
   DESTDIR=$$OUT_PWD-lib
  # /home/ingemar/PROGRAMMERING/streamcapture2/lib5
}

equals(QT_MAJOR_VERSION, 6) {
DESTDIR=$$OUT_PWD-lib

}

# UI_DIR = ../code

TRANSLATIONS += i18n/_libselectfont_sv_SE.ts \
                i18n/_libselectfont_it_IT.ts \
                i18n/_libselectfont_template_xx_XX.ts

 RESOURCES += \
    resource_selectfont_win.qrc

 RC_ICONS = images/font.ico

message (--------------------------------------------------)
message (OS: $$QMAKE_HOST.os)
message (Arch: $$QMAKE_HOST.arch)
message (Cpu count: $$QMAKE_HOST.cpu_count)
#message (Name: $$QMAKE_HOST.name)
#message (Version: $$QMAKE_HOST.version)
#message (Version string: $$QMAKE_HOST.version_string)
compiler=$$basename(QMAKESPEC)
message(compiler: $$compiler)
message (qmake path: $$QMAKE_QMAKE)
message (Qt version: $$QT_VERSION)
message(*.pro path: $$_PRO_FILE_PWD_)
message(TEMPLATE: $$TEMPLATE)
message(TARGET: $$TARGET)
message(DESTDIR: $$DESTDIR)
#message(HEADERS $$HEADERS)
#message(SOURCES: $$SOURCES)
#message(INCLUDEPATH $$INCLUDEPATH)
message(LIBS: $$LIBS)
message(PWD: $$PWD)
message (--------------------------------------------------)

